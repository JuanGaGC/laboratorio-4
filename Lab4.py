# 1-Escriba un ciclo WHILE que lea números hasta que el usuario
# lo desee y los agregue a undiccionario, posteriormente imprima
# los elementos de dicho diccionario, el key deldiccionario de ser
# generado aleatoriamente (Tema: ciclo While, Diccionario) 4 puntos
'''
num=1

import random as rd
diccionario={}

while num!=0:
    key=rd.randint(1,1000)
    num=int(input("Ingrese un numero: "))
    diccionario={key:num}
    num=num+1
    num2=input("Desea ingresar otro numero? (s/n)\n"
            "s=Si\n"
            "n=No\n")
    if num2=="n":
        break

print (diccionario)
print("Gracias por utilizar el programa")
'''

# 2-Cree una matriz de 3 x 3 y rellene los elementos con números aleatorios
# según se indica(Tema: Matrices y uso de Random) valor 20 puntos
# • La columna #1 debe tener números entre 10 y 20
# • La columna #2 debe tener números entre 21 y 30
# • La columna #3 debe tener números entre 31 y 40
# • La posición central debe ser un 99

'''
import random

filas=3
columnas=3

matriz=[[0 for i in range(filas)]for j in range(columnas)]

for i in range(filas):
    for j in range(columnas):
        columna0=random.randrange(10,20)
        columna1=random.randrange(21,30)
        columna2=random.randrange(31,40)
        matriz[i][0]=columna0
        matriz[i][1]=columna1
        matriz[i][2]=columna2
        valor=99
        matriz[1][1]=(valor)
for m in matriz:
    print(m)
'''
# 3-Al ejercicio anterior, generarle un ciclo de manera que el usuario pueda imprimir las 3
# matrices diferentes hasta que lo desee. (Tema ciclos y Procedimientos )5 puntos

'''
import random

matriz=[[0 for i in range(3)]for j in range(3)]

for i in range(3):
    for j in range(3):
        columna0=random.randrange(10,20)
        columna1=random.randrange(21,30)
        columna2=random.randrange(31,40)
        matriz[i][0]=columna0
        matriz[i][1]=columna1
        matriz[i][2]=columna2
        valor=99
        matriz[1][1]=(valor)
for m in matriz:
    print(m)
opcion=input("Desea imprimir otra matriz (s/n)\n"
                    "s=Si\n"
                    "n=No\n")
if opcion=="s":
    for i in range(3):
        for j in range(3):
            columna0=random.randrange(10,20)
            columna1=random.randrange(21,30)
            columna2=random.randrange(31,40)
            matriz[i][0]=columna0
            matriz[i][1]=columna1
            matriz[i][2]=columna2
            valor=99
            matriz[1][1]=(valor)
    for m in matriz:
        print(m)
        opcion=input("Desea imprimir otra matriz (s/n)\n"
                    "s=Si\n"
                    "n=No\n")


else:
    print("Programa finalizado")
'''


# 4-Crear la CLASE vehículo con atributos (color, marca, y precio) y realizar un proceso que
# permita guardar 3 automóviles en una lista
# Al terminar de agregar los elementos, debe poder recorrer la lista y preguntarle al usuario
# si desea cambiar el precio del vehículo, en caso afirmativo realizar el cambio respectivo y
# continuar imprimiendo hasta terminar con los demás objetos de la lista (tema: clases,
# objetos, listas, ciclos) 25 puntos
# debe hacer uso de los atributos del objeto para mostrar los mensajes como se ve en el


#opcion=input("Desea elegir las caracteristicas de un automovil? (s/n)\n"
#            "s=Si\n"
#            "n=No\n")

lista=list()
class vehiculo:
    def Caracteristicas(self,pColor,pMarca,pPrecio):
        self.color=pColor
        self.marca=pMarca
        self.precio=pPrecio

color=input("Elija un color: ")
lista.append(color)
marca=input("Elija un marca: ")
lista.append(marca)
precio=int(input("Elija un precio: "))
lista.append(precio)

color1=input("Elija un color: ")
lista.append(color1)
marca1=input("Elija un marca: ")
lista.append(marca1)
precio1=int(input("Elija un precio: "))
lista.append(precio1)

color2=input("Elija un color: ")
lista.append(color2)
marca2=input("Elija un marca: ")
lista.append(marca2)
precio2=int(input("Elija un precio: "))
lista.append(precio2)
print("El color es: ",color, "La marca es: ",marca, "y el precio es: ",precio,"\n"
      "El color es: ",color1, "La marca es: ",marca1, "y el precio es: ",precio1,"\n"
      "El color es: ",color2, "La marca es: ",marca2, "y el precio es: ",precio2)

opcion=input("Desea cambiarle el precio al vehiculo",marca,"? (s/n)\n"
            "s=Si\n"
            "n=No\n")
if opcion!="n":
    precioNuevo=int(input("Digite un precio nuevo: "))
    lista.insert(precio,precioNuevo)
    print("El color es:",color, "La marca es:",marca, "y el precio es:",precioNuevo)

opcion1=input("Desea cambiarle el precio al vehiculo",marca1,"? (s/n)\n"
            "s=Si\n"
            "n=No\n")
if opcion1!="n":
    precioNuevo1=int(input("Digite un precio nuevo: "))
    lista.insert(precio1,precioNuevo1)
    print("El color es:",color1, "La marca es:",marca1, "y el precio es:",precioNuevo1)

opcion2=input("Desea cambiarle el precio al vehiculo",marca2,"? (s/n)\n"
            "s=Si\n"
            "n=No\n")
if opcion2!="n":
    precioNuevo2=int(input("Digite un precio nuevo: "))
    lista.insert(precio2,precioNuevo2)
    print("El color es:",color2, "La marca es:",marca2, "y el precio es:",precioNuevo2)


